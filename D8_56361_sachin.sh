#!/bin/bash


# Write a menu-driven shell script with the following functionalities:

# 1. Print all Strong numbers between the given range. (Provide range as positional parameters)

# (Strong number => original number == sum of the factorial of each digit in given number (to calculate factorial, don't use any Linux command))

# 2. Print pattern. (Take the number of rows from the user)

# 3. Command 1: Write a command to print the multiplication of two floating-point numbers.

# 4. Command 2: Write a command to remove write permissions for a group of a given file.

# 5. Command 3: Write a command to convert spaces of a given string to the newline characters.

# 6. Command 4: Write a command to find a directory with a name in your home directory.

# 7. Command 5: Write a command to count the number of occurrences of a word in the file.

function strong_number  {
    #!/bin/bash

lb=$1
ub=$2

while [ $lb -lt $ub ]
do
    n=$lb
    k=$lb
    sum=0
    while [ $n -gt 0 ]
    do
        d=`echo $n%10|bc`
        fact=1
        i=1
            while [ $i -le $d ]
            do
            fact=`echo $fact*$i|bc`
            i=`echo $i+1|bc`
            done
        sum=`echo $sum+$fact|bc`
        n=`echo $n/10|bc`
    done

    if [ $sum -eq $k ]
    then
        echo "$k"
    fi

    lb=`expr $lb + 1`
done

exit#!/bin/bash

lb=$1
ub=$2

while [ $lb -lt $ub ]
do
    n=$lb
    k=$lb
    sum=0
    while [ $n -gt 0 ]
    do
        d=`echo $n%10|bc`
        fact=1
        i=1
            while [ $i -le $d ]
            do
            fact=`echo $fact*$i|bc`
            i=`echo $i+1|bc`
            done
        sum=`echo $sum+$fact|bc`
        n=`echo $n/10|bc`
    done

    if [ $sum -eq $k ]
    then
        echo "$k"
    fi

    lb=`expr $lb + 1`
done

exit



}

function Print_pattern  {

    
clear

echo -n "enter no of rows :"
read rows


i=0


while [ $i -le $rows ]
do
    j=0
    while [ $j -le $i ]
    do
      
       echo -n "*"
       j=`expr $j + 1`
    done 
    echo " "
    i=`expr $i + 1`
done


exit
  
            } 


function Multi {

        echo enter a and b
read a b
c=`echo $a+$b | bc`
echo $c


}

function Command_1 {

# accept filepath from user
echo -n "enter the filename to which execute permission is required : "
read filename
#command to give execute permission
#chmod +x script.sh(filename)

}

function Command_2 {
    
   #!/bin/bash

clear

echo -n "Enter String :"

read String

echo "$String" | tr '[:upper:]' '[:lower:]'

  
}

function Command_4 {

    #!/bin/bash

# script to find file with your name in home directory.
clear

echo -n "filename: "
read filename

 find ./GFG -name $filename

}

function Command_5 {

    grep -o -i mauris example.txt | wc -l
    
}

function menu {

    clear
    echo 
    echo -e "\t\t Select Options \n"
    echo -e "\t1. Print Strong number"
    echo -e "\t2. Print Pattern"
    echo -e "\t3. multiplication of two floating-point numbers"
    echo -e "\t4. remove write permissions"
    echo -e "\t5. convert spaces of a given string"
    echo -e "\t6. find a directory with a name"
    echo -e "\t7. count the number of occurrences"
    read -n 1 option
}

while [ 1 ]
do 
            menu
            case $option in
            0)
            break ;;
            1)
            strong_number ;;

            2)
            Print_pattern ;;

            3)
            Command_1 ;;

            4)
            Command_2 ;;

            5)
            Command_3 ;;

            6)
            Command_4 ;;

            7)
            Command_5 ;;

            *)
            clear 
            echo "Sorry , wrong selection" ;;
            esac
            echo -en "\n\n\t\t\t Hit any key to continue"
            read -n 1 line
            done
            clear

        
